﻿namespace ImageProcessing
{
    partial class Form_Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox_SourceImage = new System.Windows.Forms.PictureBox();
            this.openFileDialog_LoadImage = new System.Windows.Forms.OpenFileDialog();
            this.button_LoadImage = new System.Windows.Forms.Button();
            this.button_Mean = new System.Windows.Forms.Button();
            this.button_Med = new System.Windows.Forms.Button();
            this.label_Source = new System.Windows.Forms.Label();
            this.label_Conveted = new System.Windows.Forms.Label();
            this.pictureBox_ConvertedImage = new System.Windows.Forms.PictureBox();
            this.button_Edges = new System.Windows.Forms.Button();
            this.button_Sharpen = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_SourceImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ConvertedImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_SourceImage
            // 
            this.pictureBox_SourceImage.Location = new System.Drawing.Point(161, 27);
            this.pictureBox_SourceImage.Name = "pictureBox_SourceImage";
            this.pictureBox_SourceImage.Size = new System.Drawing.Size(256, 256);
            this.pictureBox_SourceImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_SourceImage.TabIndex = 0;
            this.pictureBox_SourceImage.TabStop = false;
            // 
            // openFileDialog_LoadImage
            // 
            this.openFileDialog_LoadImage.FileName = "*.bmp;*.jpg;*.png";
            // 
            // button_LoadImage
            // 
            this.button_LoadImage.Location = new System.Drawing.Point(12, 12);
            this.button_LoadImage.Name = "button_LoadImage";
            this.button_LoadImage.Size = new System.Drawing.Size(110, 23);
            this.button_LoadImage.TabIndex = 1;
            this.button_LoadImage.Text = "Load Image...";
            this.button_LoadImage.UseVisualStyleBackColor = true;
            this.button_LoadImage.Click += new System.EventHandler(this.button_LoadImage_Click);
            // 
            // button_Mean
            // 
            this.button_Mean.Location = new System.Drawing.Point(12, 41);
            this.button_Mean.Name = "button_Mean";
            this.button_Mean.Size = new System.Drawing.Size(110, 23);
            this.button_Mean.TabIndex = 2;
            this.button_Mean.Text = "Mean";
            this.button_Mean.UseVisualStyleBackColor = true;
            this.button_Mean.Click += new System.EventHandler(this.button_Mean_Click);
            // 
            // button_Med
            // 
            this.button_Med.Location = new System.Drawing.Point(12, 70);
            this.button_Med.Name = "button_Med";
            this.button_Med.Size = new System.Drawing.Size(110, 23);
            this.button_Med.TabIndex = 3;
            this.button_Med.Text = "Median";
            this.button_Med.UseVisualStyleBackColor = true;
            this.button_Med.Click += new System.EventHandler(this.button_Med_Click);
            // 
            // label_Source
            // 
            this.label_Source.AutoSize = true;
            this.label_Source.Location = new System.Drawing.Point(159, 12);
            this.label_Source.Name = "label_Source";
            this.label_Source.Size = new System.Drawing.Size(77, 12);
            this.label_Source.TabIndex = 4;
            this.label_Source.Text = "Source Image";
            // 
            // label_Conveted
            // 
            this.label_Conveted.AutoSize = true;
            this.label_Conveted.Location = new System.Drawing.Point(434, 12);
            this.label_Conveted.Name = "label_Conveted";
            this.label_Conveted.Size = new System.Drawing.Size(95, 12);
            this.label_Conveted.TabIndex = 5;
            this.label_Conveted.Text = "Converted Image";
            // 
            // pictureBox_ConvertedImage
            // 
            this.pictureBox_ConvertedImage.Location = new System.Drawing.Point(436, 27);
            this.pictureBox_ConvertedImage.Name = "pictureBox_ConvertedImage";
            this.pictureBox_ConvertedImage.Size = new System.Drawing.Size(256, 256);
            this.pictureBox_ConvertedImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ConvertedImage.TabIndex = 6;
            this.pictureBox_ConvertedImage.TabStop = false;
            // 
            // button_Edges
            // 
            this.button_Edges.Location = new System.Drawing.Point(12, 99);
            this.button_Edges.Name = "button_Edges";
            this.button_Edges.Size = new System.Drawing.Size(110, 23);
            this.button_Edges.TabIndex = 7;
            this.button_Edges.Text = "Edges";
            this.button_Edges.UseVisualStyleBackColor = true;
            this.button_Edges.Click += new System.EventHandler(this.button_Edges_Click);
            // 
            // button_Sharpen
            // 
            this.button_Sharpen.Location = new System.Drawing.Point(12, 128);
            this.button_Sharpen.Name = "button_Sharpen";
            this.button_Sharpen.Size = new System.Drawing.Size(110, 23);
            this.button_Sharpen.TabIndex = 8;
            this.button_Sharpen.Text = "Sharpen";
            this.button_Sharpen.UseVisualStyleBackColor = true;
            this.button_Sharpen.Click += new System.EventHandler(this.button_Sharpen_Click);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 328);
            this.Controls.Add(this.button_Sharpen);
            this.Controls.Add(this.button_Edges);
            this.Controls.Add(this.pictureBox_ConvertedImage);
            this.Controls.Add(this.label_Conveted);
            this.Controls.Add(this.label_Source);
            this.Controls.Add(this.button_Med);
            this.Controls.Add(this.button_Mean);
            this.Controls.Add(this.button_LoadImage);
            this.Controls.Add(this.pictureBox_SourceImage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Main";
            this.Text = "Image Processing";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_SourceImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ConvertedImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_SourceImage;
        private System.Windows.Forms.OpenFileDialog openFileDialog_LoadImage;
        private System.Windows.Forms.Button button_LoadImage;
        private System.Windows.Forms.Button button_Mean;
        private System.Windows.Forms.Button button_Med;
        private System.Windows.Forms.Label label_Source;
        private System.Windows.Forms.Label label_Conveted;
        private System.Windows.Forms.PictureBox pictureBox_ConvertedImage;
        private System.Windows.Forms.Button button_Edges;
        private System.Windows.Forms.Button button_Sharpen;
    }
}

